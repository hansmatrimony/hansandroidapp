import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

import { RegisterSdPage } from '../register-sd/register-sd'

@Component({
  selector: 'page-register-cd',
  templateUrl: 'register-cd.html',
})
export class RegisterCdPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  goToSD(){
    this.navCtrl.push(RegisterSdPage);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RegisterCdPage');
  }

}
