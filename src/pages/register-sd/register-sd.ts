import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

import { RegisterLdPage } from '../register-ld/register-ld';

@Component({
  selector: 'page-register-sd',
  templateUrl: 'register-sd.html',
})
export class RegisterSdPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  goToLD()
  {
    this.navCtrl.push(RegisterLdPage);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RegisterSdPage');
  }

}
