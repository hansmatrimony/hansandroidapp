import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

import { VerifyNumberPage } from '../verify-number/verify-number'

@Component({
  selector: 'page-register-ld',
  templateUrl: 'register-ld.html',
})
export class RegisterLdPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  passwordType : string = 'password';
  passwordIcon : string = 'eye-off';

  togglePassword()
  {
    this.passwordType = this.passwordType === 'text' ? 'password' : 'text';
    this.passwordIcon = this.passwordIcon === 'eye-off' ? 'eye' : 'eye-off';
  }

  verifyNumber(){
    this.navCtrl.setRoot(VerifyNumberPage);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RegisterLdPage');
  }

}
