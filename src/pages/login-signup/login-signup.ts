import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

import { LoginPage } from '../login/login';
import { RegisterPfPage } from '../register-pf/register-pf';
import { Facebook, FacebookLoginResponse } from '@ionic-native/facebook';

@Component({
  selector: 'page-login-signup',
  templateUrl: 'login-signup.html',
})
export class LoginSignupPage {

  constructor(public navCtrl: NavController, public navParams: NavParams, private fb: Facebook) {
  }

  goToLogin(){
    this.navCtrl.push(LoginPage);
  }


  login(){
    console.log("hello");
    this.fb.login(['public_profile', 'email'])
  .then((res: FacebookLoginResponse) => console.log('Logged into Facebook!', res))
  .catch(e => console.log('Error logging into Facebook', e));
  }

  goToRegister(){
    this.navCtrl.push(RegisterPfPage);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginSignupPage');
  }

}
