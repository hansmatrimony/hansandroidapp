import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

import { RegisterPdPage } from '../register-pd/register-pd';

@Component({
  selector: 'page-register-pf',
  templateUrl: 'register-pf.html',
})
export class RegisterPfPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {

  }

  goToPD(){
    this.navCtrl.push(RegisterPdPage);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RegisterPfPage');
  }

}
