import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

import { RegisterCdPage } from '../register-cd/register-cd';

@Component({
  selector: 'page-register-pd',
  templateUrl: 'register-pd.html',
})
export class RegisterPdPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  goToCD(){
    this.navCtrl.push(RegisterCdPage);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RegisterPdPage');
  }

}
