import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { Facebook, FacebookLoginResponse } from '@ionic-native/facebook';
import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { LoginSignupPage } from '../pages/login-signup/login-signup';
import { LoginPage } from '../pages/login/login';
import { RegisterPfPage } from '../pages/register-pf/register-pf';
import { RegisterPdPage } from '../pages/register-pd/register-pd';
import { RegisterCdPage } from '../pages/register-cd/register-cd';
import { RegisterSdPage } from '../pages/register-sd/register-sd';
import { RegisterLdPage } from '../pages/register-ld/register-ld';
import { VerifyNumberPage } from '../pages/verify-number/verify-number';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    LoginSignupPage,
    LoginPage,
    RegisterCdPage,
    RegisterLdPage,
    RegisterPdPage,
    RegisterPfPage,
    RegisterSdPage,
    VerifyNumberPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    LoginSignupPage,
    LoginPage,
    RegisterCdPage,
    RegisterLdPage,
    RegisterPdPage,
    RegisterPfPage,
    RegisterSdPage,
    VerifyNumberPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    Facebook,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
